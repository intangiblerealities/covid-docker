
export function getGcpLocations() {
  return [
    { group: 'Asia', region: 'asia-south1', display: 'Mumbai' },
    { group: 'Asia', region: 'asia-northeast3', display: 'Seoul' },
    { group: 'Asia', region: 'asia-southeast1', display: 'Singapore' },
    { group: 'Asia', region: 'asia-east1', display: 'Taiwan' },
    { group: 'Asia', region: 'asia-northeast1', display: 'Tokyo' },
    { group: 'Europe', region: 'europe-west3', display: 'Frankfurt' },
    { group: 'Europe', region: 'europe-west2', display: 'London' },
    { group: 'Europe', region: 'europe-west4', display: 'Netherlands' },
    { group: 'North America', region: 'us-central1', display: 'Iowa' },
    { group: 'North America', region: 'us-east4', display: 'North Virginia' },
    { group: 'North America', region: 'us-west1', display: 'Oregon' },
    { group: 'North America', region: 'us-east1', display: 'South Carolina' },
    { group: 'South America', region: 'southamerica-east1', display: 'Sao Paolo' }
  ]
}